"""
Задание 5.4

Импортируйте функцию из задания 4.4а
При помощи конструкции assert напишите тесты, которые будут выполняться при запуске скрипта
    (тесты не должны выполняться при импорте вашего скрипта)
Набор тестовых данных для проверки задан в списке check_list

"""

import sys
sys.path.append('C:\\Users\\Ильмира\\git_projects\\exercises_april_2020_my\\04_files_and_func')
import task_4_4a



check_list = [
                (1, '+', 1, 2),
                (10, '-', 3, 7),
                (2, '/', 0, False),
                (2, '*', 0, 0),
                (0, '-', 5, -5),
                (2, '+', 'ger', False),
                ('10', '+', '7', False),
                (100, '/', 5, 20.0),
                (5, 2, '+', False),
                (54, '+-', 6, False),
                (5, '+', -5, 0),
                (4, ' + ', 5, 9)
]

for operation in check_list:
    assert task_4_4a.calculator(operation[0], operation[1], operation[2]) == operation[3]

