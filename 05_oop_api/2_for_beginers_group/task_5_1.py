"""
Задание 5.1

Добавьте к классу Contact свойства address и birthday
Создайте объект mike типа Contact с такими данными:
имя: Михаил Булгаков
телефон: 2-03-27
адрес: Россия, Москва, Большая Пироговская, дом 35б, кв. 6
день рождения: 15.05.1891
Создайте объект vlad типа Contact с такими данными:
имя: Владимир Маяковский
телефон: 73-88
адрес: Россия, Москва, Лубянский проезд, д. 3, кв. 12
день рождения: 19.07.1893
Вызовите функцию print_contact(), чтобы напечатать на экране свойства созданных объектов


"""



class Contact:
    def __init__(self, name, phone, address, birthday):
        self.name = name
        self.phone = phone
        # добавьте свойство address
        # добавьте свойство birthday
        self.address = address
        self.birthday = birthday
        print(f"Создаём новый контакт {name}")

# здесь создайте объекты mike и vlad
mike = Contact('Михаил Булгаков', '2-03-27', 'Россия, Москва, Большая Пироговская, дом 35б, кв. 6', '15.05.1891')
vlad = Contact('Владимир Маяковский', '73-88', 'Россия, Москва, Лубянский проезд, д. 3, кв. 12', '19.07.1893')

def print_contact():
    print(f"{mike.name} — адрес: {mike.address}, телефон: {mike.phone}, день рождения: {mike.birthday}")
    print(f"{vlad.name} — адрес: {vlad.address}, телефон: {vlad.phone}, день рождения: {vlad.birthday}")

# здесь вызовите функцию print_contact(), 
# и она напечатает на экране данные контактов mike и vlad
print_contact()