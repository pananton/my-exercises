# -*- coding: utf-8 -*-
'''
Задание 2.1d

Переделать скрипт из задания 2.1c таким образом, чтобы, при запросе параметра,
пользователь мог вводить название параметра в любом регистре.

Пример выполнения скрипта:
$ python task_2_1d.py
Введите имя сервера: dev
Введите имя параметра (os, model, vendor, location, ip): OS
centos


Ограничение: нельзя изменять словарь london_co.

Все задания надо выполнять используя только пройденные темы.
То есть эту задачу можно решить без использования условия if.
'''

servers = {
    'dev': {
        'location': 'Лубянка',
        'vendor': 'IBM',
        'model': 'i2570',
        'os': 'centos',
        'ip': '10.255.0.1'
    },
    'qa': {
        'location': 'поеображенка',
        'vendor': 'Cisco',
        'model': '4451',
        'os': 'centos',
        'ip': '10.255.0.2'
    },
    'prod': {
        'location': 'технопарк',
        'vendor': 'Dell',
        'model': '3850',
        'os': 'centos',
        'ip': '10.255.0.101',
        'vlans': '10,20,30',
        'routing': True
    }
}

server = input('Введите имя сервера: ')
server_param = list(servers[server].keys())
param = input('Введите имя параметра (' + ', '.join(server_param) + '): ')
dict_param = servers[server]
print(dict_param.get(param.lower(), 'Такого параметра нет'))