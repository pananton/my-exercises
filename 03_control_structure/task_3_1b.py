'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.

'''

calc = input('Введите выражение: ')
while calc:
    oper_list = ['+', '-', '*', '/']
    match = False
    for operator in oper_list:
        if operator in calc:
            if operator == '+':
                a1 = int(calc.split('+')[0])
                a2 = int(calc.split('+')[1])
                res = a1 + a2
                print('Результат сложения: {}'.format(res))
                match = True
                break
            elif operator == '-':
                a1 = int(calc.split('-')[0])
                a2 = int(calc.split('-')[1])
                res = a1 - a2
                print('Результат вычетания: {}'.format(res))
                match = True
                break
            elif operator == '*':
                a1 = int(calc.split('*')[0])
                a2 = int(calc.split('*')[1])
                res = a1 * a2
                print('Результат умножения: {}'.format(res))
                match = True
                break
            elif operator == '/':
                a1 = int(calc.split('/')[0])
                a2 = int(calc.split('/')[1])
                res = a1 / a2
                print('Результат деления: {}'.format(res))
                match = True
    if match == False:
        print('Невалидное сообщение')
    calc = input('Введите выражение: ')
