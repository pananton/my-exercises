"""
Здание 4.4

Написать 4 функции
plus_args(v1, v2)   # для сложения aргументов v1 + v2
minus_args(v1, v2)  # для вычетания aргументов v1 - v2
multi_args(v1, v2)  # для умножения аргументов v1 * v2
div_args(v1, v2)    # для деления аргументов v1 / v2

пример запуска функции plus_args :

print(plus_args(5, 9))
Операция - сложение: 5 + 9
14
"""

def plus_args(v1, v2):
    '''
    для сложения aргументов v1 + v2
    :param v1: первый аргумент арифметической операции
    :param v2: второй аргумент арифметической операции
    :return result: вывод результата операции
    '''
    result = v1 + v2
    print('Операция - сложение: {} + {}'.format(v1,v2))
    #result = 'Операция - сложение: {} + {}'.format(v1,v2)
    return result

def minus_args(v1, v2):
    '''
    для вычетания aргументов v1 - v2
    :param v1: первый аргумент арифметической операции
    :param v2: второй аргумент арифметической операции
    :return result: вывод результата операции
    '''
    result = v1 - v2
    print('Операция - вычитание: {} - {}'.format(v1,v2))
    return result

def multi_args(v1, v2):
    '''
    для умножения аргументов v1 * v2
    :param v1: первый аргумент арифметической операции
    :param v2: второй аргумент арифметической операции
    :return result: вывод результата операции
    '''
    result = v1 * v2
    print('Операция - умножение: {} * {}'.format(v1,v2))
    return result

def div_args(v1, v2):
    '''
    для деления аргументов v1 / v2
    :param v1: первый аргумент арифметической операции
    :param v2: второй аргумент арифметической операции
    :return result: вывод результата операции
    '''
    if v2 == 0:
        print('На ноль делить нельзя!')
        return False
    result = v1 / v2
    print('Операция - деление: {} / {}'.format(v1,v2))
    return result

if __name__ == '__main__':
    print(plus_args(5, 9.2))
    print(minus_args(2, 5))
    print(multi_args(3,3))
    print(div_args(1, 0))
