"""
Здание 4.4a
Cкопируйте функции написаные в предыдущем задании
Написать функции:

validate_operator(operator)
    функция проверяет, что operator равен допустимым значениям
        допустимые значения: + - * /
    Если оператор принимает допустимое значение:
        возвращается True
    Если оператор НЕ принимает допустимое значение:
        выводится строка "Неизвестный оператор"
        И возвращается False

validate_value(var)
    Функция проверяет что значение val либо int либо float
    Если все верно - возвращается True
    Если оператор равен "/" а второй операнд равен нулю:
        Выводится строка: "На ноль делить нельзя!"
        И возвращается - False
    В остальных случаях:
        Выводится строка: "Неподдерживаемый тип опреранда"
        И возвращается - False
    
calculator(v1 :int or float , operator :str, v2 :int or float)

При запуске функции:
    Проверяются оператор и операнды функциями:
        validate_operator
        validate_value
В зависимости от оператора выплнить соответствующую функцию из предыдучего задания
    Результат выполнения записать в переменную result и вернуть в конце
"""

from task_4_4 import plus_args, minus_args, multi_args, div_args

def validate_operator(operator):
    #global result
    if operator in ['+','-','*','/']:
        return True
    else:
        print('Неизвестный оператор')
        return False

def validate_value(v1,v2,operator):
    #global result
    if (type(v1) is int or type(v1) is float) and (type(v2) is int or type(v2) is float):
        #if operator == '/' and v2 == '0':
        #    return False
        #    print('На ноль делить нельзя!')
        #print('1')
        return True
        print('2')
    else:
        print('Неподдерживаемый тип опреранда')
        return False

def operation(v1,v2,operator):
    if operator == '+':
        return plus_args(v1, v2)
    elif operator == '-':
        return minus_args(v1, v2)
    elif operator == '*':
        return multi_args(v1, v2)
    else:
        return div_args(v1, v2)

def calculator(v1,operator,v2):
    #global result
    check_oper = validate_operator(operator)
    check_value = validate_value(v1,v2,operator)
    if check_oper and check_value:
        result = operation(v1,v2,operator)
        return result
    else:
        return False

#result = ''

# добавил исключение для задания 5_4
if __name__ == '__main__':
    print(calculator(0.5,'&',2))