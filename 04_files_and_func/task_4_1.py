# -*- coding: utf-8 -*-
'''
Задание 4.1

Обработать строки из файла servers.txt
и вывести информацию по каждому серверу в таком виде:

server:            qa_test1
ip:                10.0.13.3
switch_port:       FastEthernet0/0
uptime:            3d20h

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
f = open('servers.txt','r')
a = f.read().split("\n")
for i in a:
    b = i.split(", ")[0].split(" ")[8::30] + i.split(", ")[0].split(" ")[-1:] + i.split(", ")[-2:]
    print('server:            {}'.format(b[0]))
    print('ip:                {}'.format(b[1]))
    print('switch_port:       {}'.format(b[3]))
    print('uptime:            {}'.format(b[2]))
    print('\n')

f.close()