"""
Задание 4.2a
Напишите функцию, которая вычисляет количество дней в году (учитывать високосный год).
      На вход подается год.
      Функция возвращает кол-во дней
Используйте функцию из предыдущего задания, для проверки является ли год високосным.
"""

from task_4_2 import CheckYear

def countdays(leapyear):
    print(leapyear)
    if leapyear:
        return 366 #исправил путаницу с возвращаемыми днями
    else:
        return 365

leapyear = CheckYear(int(input('Введите год: ')))
days = countdays(leapyear)
print('В году {} дней '.format(days))

