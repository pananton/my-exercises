"""
Задание 4.2
Напишите функцию, которая проверяет является ло передаваемый ей год високосным или нет.
      'На вход подается год. 
      Функция должна возвращать:
        True - високосный, 
        False - обычный'
"""

def CheckYear(year):
    '''
    Функция проверяет високосный ло год
    :param year - год для проверки
    :return возвращает True - високосный, False - обычный'
    '''
    if year % 4 == 0:
        if year % 100 != 0 or year % 400 == 0:  #исправил ошибку в условиях
            return True
        else:
            return False
    else:
        return False

if __name__ == '__main__':
    leapyear = CheckYear(int(input('Введите год: ')))
    print(leapyear)