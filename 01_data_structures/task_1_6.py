# -*- coding: utf-8 -*-
'''
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

vova = 'O Владимир       15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'
vova = vova.replace(',', '').replace('/', ' ').split()
#print(vova.replace(',', '').replace('/', ' ').split())
print('name:\t', vova[1], '\nip:\t\t', vova[4], '\nгород:\t', vova[6], '\ndate:\t', vova[2], '\ntime:\t', vova[3])